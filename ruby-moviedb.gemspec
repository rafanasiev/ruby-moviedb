Gem::Specification.new do |s|
  s.name        = 'ruby-moviedb'
  s.version     = '0.0.0'
  s.date        = '2015-05-24'
  s.summary     = "moviedb"
  s.description = "A simple moviedb tool"
  s.authors     = ["Ruslan A. Afanasiev"]
  s.email       = 'ruslan.afanasiev@gmail.com'
  s.files       = ["bin/run_db.rb", "lib/film.rb", "lib/utils.rb", "lib/format.rb"]
  s.homepage    = 'http://rubygems.org/gems/ruby-moviedb'
  s.license     = 'MIT'
end
