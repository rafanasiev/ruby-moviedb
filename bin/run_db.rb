#!/usr/bin/env ruby
#
# run_db.rb - a tool to operate with MovieDB in two ways:
#               * a command line args
#               * an interactive user minu via CLI
#
#   Author::    Ruslan A. Afanasiev  (mailto:ruslan.afanasiev@gmail.com)
#   License::   Distributes under the same terms as Ruby
# -------------------------------------------------------------------
# -*- encoding: utf-8 -*-

require 'getoptions'
require 'pp'
require '../lib/film'
require '../lib/utils'

def usage
    print <<USAGE
Usage: #{$0} [OPTION]... [ARG]
Operates with a small movie DB, search by title, star name etc.

        -i, --import        import movies from a data source txt file
        -a, --add           add a record. The argument string has to be:
                            #{$0} --add 'year=YYYY;title=The movie;format=DVD;stars=John Doe,Mia Lee'
                            NOTE: the argument string should be quoted!
        -d, --delete        delete a movie (script asks movie Id)
        -p, --property      show properties of a movie
        -t  --titles        show list title and id of all movies, alphabetically
        -y, --years         show list title, id and year of all movies, chronologically
            --find-by-title find movies by titles (script asks for a search string)
            --find-by-star  find movies by star (script asks for a star name)
        -m, --menu          script will prompt an interactive menu to add a new record, etc

            --help  display this help and exit

USAGE
    exit(0)
end


## check command line agrs

ARGV << "-h" if ARGV.empty?

## define CLI arguments and options
opt = begin
  GetOptions.new(
    %w{help usage debug! import=s menu add=s delete=i property=i titles years find-by-title find-by-star}
  )
rescue GetOptions::ParseError => e
  puts "CL argument error - " + e.message
  usage
end

## check CLI agrs
# - print usage if help OR usage
usage if opt.usage or opt.help

#
# - import
#

Film.import(opt.import, Dir.pwd, opt.debug) if opt.import

#
# - execute interactive menu
#

if opt.menu
    puts "! NOT IMPLEMENTED YET !"
    exit(0)
end

#
# - add a movie
#

if opt.add
  puts " -- Adding a new movie record ..."
  args = {}

  opt.add.split(';').each do |i|
    k, v = i.split('=')
    if k == 'stars'
      args[k] = v.split(',')
    else
      args[k] = v
    end
  end

  Film.add_movie(args, Dir.pwd, opt.debug)

  exit(0)
end

#
# - delete a movie by Id
#

if opt.delete
  movie_id = opt.delete

  puts " -- Are you sure to delete movie with Id <#{movie_id}>?"
  print "> Input yes/no: "
  answer = gets.chomp

  if answer.downcase.eql?('yes')
    Film.del_movie(movie_id, Dir.pwd, opt.debug)
  end

  exit(0)

end

#
# - find movie by Id
#

if opt.property
  puts " -- Looking a movie by Id <#{opt.property}>"

  result = Film.grep_by_attr(:id, opt.property, Dir.pwd, opt.debug)

  if result.empty?
      puts " -- No movie was found"
      exit(1)
  end
  puts result[0]
  exit(0)
end

#
# - find movie by Title or by Star's firstname|lastname
#

if opt['find-by-title'] or opt['find-by-star']
  print "> Input movie's title (or a word): " if opt['find-by-title']
  print "> Input star's name (or surname): " if opt['find-by-star']
  attribute = ( opt['find-by-title'] ) ? :title : :star
  value = gets.chomp
  puts " -- Start searching movies by [#{attribute}]"
  movies = Film.grep_by_attr(attribute, value, Dir.pwd, opt.debug)

  if movies.empty?
    puts " -- No movies were found"
    exit(1)
  else
    movies.each { |o| puts o }
    exit(0)
  end
end

#
# - show movies, searching by attribute
#   * ordered alphabetically if Title
#   * ordered chronologically if Year
#

if opt.titles or opt.years
  attribute = (opt.titles) ? :titles : :years

  case attribute
  when :titles
    puts ' -- Show all movies (Id), ordered alphabetically:'
  else
    puts ' -- Show all movies (Id, Year), ordered chronologically:'
  end

  movies = Film.sort_by_attr(attribute, Dir.pwd, opt.debug)

  if opt.titles
    movies.each {|o| puts " - [%02d] -> '%s'" % [o.id, o.title] }
  else
    movies.each {|o| puts " - [%02d] -> %d - '%s'" % [o.id, o.year, o.title] }
  end
end
