module ReleaseYear

  def check_release_year(year)
    year =~ /^\d{4}$/
  end

end
