module Format
    @@formats = %w{VHS DVD Blu-Ray}

    def check_format(format)
        @@formats.include?(format)
    end
end
