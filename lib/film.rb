# Author::    Ruslan A. Afanasiev  (mailto:ruslan.afanasiev@gmail.com)
# License::   Distributes under the same terms as Ruby

## add current dir to $LOAD_PATH
libdir = File.expand_path(File.dirname(__FILE__))
$LOAD_PATH.unshift(libdir) unless $LOAD_PATH.include?(libdir)

require '../lib/format'
require '../lib/star'
require '../lib/utils'
require '../lib/release_year'
require 'singleton'

##
# class IdCounter - tracks Object Ids during import action
##
class IdCounter
    include Singleton

    def initialize
        @id = 0
    end

    def get_id
        @id += 1
        @id
    end
end

##
# class Movie - a simple class to represent Movie object
##
#

class Movie
  include Star, Format, ReleaseYear

  attr_accessor :id, :title, :year, :format, :stars

  # Initialize Movie object
  #
  # @param [Hash] args
  #
  # @return
  #
  def initialize(args)
    args.each do |k,v|
        # name - get firstname, midname and surname
        if k.eql?(:stars) or k.eql?('stars')
          @stars = []
          v.each { |s| @stars << Star::Actor.new(s) }
          next
        end
        # check release year
        if k.eql?(:year)
          unless check_release_year(v)
            raise ArgumentError, "Import error - wrong rel.year format <#{v}>!"
          end
        end
        # check movie's format
        if k.eql?(:format)
            # bail out - wrong format
            unless check_format(v)
                raise ArgumentError, "Import error - wrong movie's format <#{v}>!"
            end
        end
        instance_variable_set("@#{k}", v)
    end

    # NOTE: restoration of Movie object from dump does not require Id generation
    unless @id
        @id = IdCounter.instance.get_id
    end
  end

  # Pretty print of Film instance
  #
  # @param
  #
  # @return
  #
  def to_s

    <<-EOL
*********************************************************************
        Id: #{@id}
     Title: #{@title}
Release Year: #{@year}
    Format: #{@format}
     Stars: #{@stars.inject('') { |name, o| name += o.get_star_names }}
*********************************************************************
      EOL
  end


end

##
# class Film - provides a set of class methods to import/add/etc DB records
##
class Film
    include Utils

    # Class method to envelope Utils::import
    #
    # @param [String] filename
    # @param [String] current working dir
    # @param [Bolean] debug
    #
    # @return
    #
    def self.import(filename, cwd, debug)
      movies = []

      puts " -- Start import from file: #{filename}"

      for i in Utils.import(filename) do
        if debug
          puts ' - Convert into Movie:'
          i.each {|k,v| puts "\t #{k} => #{v}"}
        end
        ## make Moviedb object
        movies << Movie.new(i)
      end

      movies.each {|m| pp m} if debug

      ## save list into YAML
      dump = Utils.data2yaml(movies, cwd)

      puts " -- Import done. Data stored into => #{dump}"

    end

    # Add new Film object
    #
    # @param [Hash] args
    # @param [String] pwd, directory path to YAML file
    # @param [Bolean] debug
    #
    # @return
    #

    def self.add_movie(args, cwd, debug)
      m = Movie.new(args)
      # get objects from YAML file
      movies = Utils.yaml2data(cwd)
      # get a movie with max Id, increase Id for new movie object
      m.id += movies.max{ |a, b| a.id <=> b.id }.id

      puts ' --- Created new Movie object:', m
      movies << m

      ## save updated list into YAML
      dump = Utils.data2yaml(movies, cwd)

      puts " -- Update done. Data stored into => #{dump}"

    end

    # Delete Film object by its Id
    #
    # @param [Fixnum] id
    # @param [String] pwd, directory path to YAML file
    # @param [Bolean] debug
    #
    # @return
    #

    def self.del_movie(movie_id, cwd, debug)
      # get objects from YAML file
      movies = Utils.yaml2data(cwd)
      # search movie by Id
      m = movies.find { |o| o.id == movie_id}

      unless m.nil?
        puts " --- Found movie with Id <#{m.id}>: "
        puts m
        movies.delete(m)
        ## save updated list into YAML
        path_to_file = Utils.data2yaml(movies, cwd)

        puts " -- Update done. Data stored into => #{path_to_file}"
      else
        puts " --- There are no movies with Id <#{movie_id}>"
      end

    end

    #
    # Find Film object by its Attribute
    #
    # @param [String] attribute
    # @param [String] value
    # @param [String] pwd, directory path to YAML file
    # @param [Bolean] debug
    #
    # @return [Array] list of objects
    #
    def self.grep_by_attr(attr, value, pwd, debug)
      results = []
      # get objects from YAML file
      movies = Utils.yaml2data(pwd)

      # looking for movies by an attribute, e.g. - id, title, star
      movies.each do |o|
        case attr
        when :id
          results << o if o.id == value
        when :title

          if o.title.downcase.include?(value.downcase)
            puts " --- Found title: #{o.title}" if debug
            results << o
          end

        when :star
          o.stars.each do |star|
            if star.exists_star?(value)
              star.name.gsub!(/^/, '>> ')
              star.surname.gsub!(/$/, ' <<')
              results << o
            end
          end

        else
          # bail out if attr is unknown
          puts " -- Unknown attribute <@#{attr}>"
          exit(1)
        end

      end

      return results
    end

    #
    # Return all Title, ID ordered by attribute (e.g by title, year)
    #
    # @param [String] attribute
    # @param [String] pwd, directory path to YAML file
    # @param [Bolean] debug
    #
    # @return [Array] { id => title }
    #
    def self.sort_by_attr(attr, pwd, debug)
        # get objects from YAML file
        movies = Utils.yaml2data(pwd)
        case attr
        when :titles
          movies.sort_by { |m| m.title }
        when :years
          movies.sort_by { |m| m.year }
        else
            # bail out if attr is unknown
            puts " -- Unknown attribute <@#{attr}>"
            exit(1)
        end
    end

end
