module Star

  class Actor
    attr_accessor :name, :midname, :surname

    def initialize(names)
      tmp = names.split(/\s/)
      @name = tmp[0]
      @midname = (tmp[1...-1].empty?) ? nil : tmp[1...-1]
      @surname = tmp[-1]
    end

    def exists_star?(patern)
      if @name =~ /#{patern}/i
        return true
      elsif @surname =~ /#{patern}/i
        return true
      elsif not @midname.nil?
        @midname.find { |mid| mid =~ /#{patern}/i }
      else
        return nil
      end
    end

    def get_star_names
        if @midname
            return "\n\t ~ #{@name} #{@midname.join(' ')} #{@surname}"
        else
            return "\n\t ~ #{@name} #{@surname}"
        end
    end

  end

end
