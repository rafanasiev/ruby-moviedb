# Author::    Ruslan A. Afanasiev  (mailto:ruslan.afanasiev@gmail.com)
# License::   Distributes under the same terms as Ruby

require "yaml"

module Utils

    DUMP_FILE = 'moviedb.yaml'

    def self.import(filename)
        data = []
        f = File.open(filename, 'r')

        begin
            tmp = {}
            f.each_line do |line|
                # empty line - one record is completed, clean up tmp Hash
                if line.match(/^$/)
                    data << tmp unless tmp.empty?
                    # clean up temp Hash
                    tmp = {}
                end

                line.chomp!
                ## slit a line to get - title, release year, format, stars
                # - trick for title - there can be ':' char
                if line.start_with?('Title')
                    title = line.split(':')[1..-1]
                    if title.size > 1
                        tmp[:title] = title.join(': ').strip
                    else
                        tmp[:title]  = title[0].strip
                    end
                end
                tmp[:year]   = line.split(':')[1].strip if line.start_with?('Release Year')
                tmp[:format] = line.split(':')[1].strip if line.start_with?('Format')
                tmp[:stars]  = line.split(':')[1].split(',').map(&:strip) if line.start_with?('Stars')

            end
        rescue SystemCallError
            $stderr.print "IO error: " + $!
            raise
        ensure
            f.close unless f.nil?
        end

        return data

    end

    def self.data2yaml(list, pwd)
        puts " -- Save data into YAML file #{DUMP_FILE}"

        begin
            path = pwd + '/' + DUMP_FILE.to_s
            File.open(path, 'w') do |f|
                list.each {|o| f.puts YAML::dump(o); f.puts "" }
            end
        rescue SystemCallError
            $stderr.print "IO error: " + $!
            raise
        ensure
            return path
        end
    end

    def self.yaml2data(pwd)
        puts " -- Restoring data from YAML file #{pwd}/#{DUMP_FILE}"

        $/="\n\n"

        begin
            movies = []
            path = pwd + '/' + DUMP_FILE.to_s

            File.open(path, 'r').each do |line|
                movies << YAML::load(line)
            end
            puts " -- Restoration - DONE"
            return movies
        rescue SystemCallError
            $stderr.print "IO error: " + $!
            raise
        end
    end

end
